import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    eliminarPrivilegioSeleccionado: Object,
    agregarPrivilegioSeleccionado: Object,
    eliminarRolSeleccionado: Object,
    agregarRolSeleccionado: Object,
    privilegiosNoAsignados: [],
    privilegiosAsignados: [],
    user: Array<Object>[],
    rol: Array<Object>[],
    rolesNoAsignados: [],
    rolesAsignados: [],
    privilegios: [],
    usuarios: [],
    roles: [],
    usuario: null,
    role: null,
    token: null,
    loggedIn: null
  },
  getters: {
    /*loggedIn(state) {
      return state.token !== null
    }*/
  },
  mutations: {
    /*retrieveToken(state, token) {
      state.token = token;
    },
    destroyToken(state) {
      state.token = null
    }*/
  },
  actions: {
    /*retrieveToken(context, credentials) {
      return new Promise((resolve, reject) => {
        axios.post('http://localhost:8080/api/login', {
          name: credentials.name,
          password: credentials.password
        }).then(respuesta => {
          const token = respuesta.data;
          localStorage.setItem('access_token', token);
          context.commit('retrieveToken', token);
          resolve(respuesta);
        }).catch(error => {
          console.log(error);
          reject(error);
        });
      });
    },
    destroyToken(context) {
      if (context.getters.loggedIn) {
        return new Promise((resolve, reject) => {
          axios.post('http://localhost:8080/api/logout').then(respuesta => {
            delete axios.defaults.headers.common["Authorization"];
            localStorage.removeItem('access_token');
            context.commit('destroyToken');
            resolve(respuesta);
          }).catch(error => {
            delete axios.defaults.headers.common["Authorization"];
            localStorage.removeItem('access_token');
            context.commit('destroyToken');
            reject(error);
          });
        });
      }
    }*/
  }
})
