import User from './components/User.vue'
import UserAux from './components/UserAux.vue'
import Role from './components/Role.vue'
import RoleAux from './components/RoleAux.vue'
import Privilege from './components/Privilege.vue'
import PrivilegeAux from './components/PrivilegeAux.vue'
import Login from './components/Login.vue'
import Logout from './components/Logout.vue'

export const routes = [
  { path: '', component: User },
  { path: '/usuarios', component: User },
  { path: '/usuarios/registro', component: UserAux },
  { path: '/usuarios/:id', component: UserAux },
  { path: '/roles', component: Role },
  { path: '/roles/agregar', component: RoleAux },
  { path: '/roles/:id', component: RoleAux },
  { path: '/privilegios', component: Privilege },
  { path: '/privilegios/agregar', component: PrivilegeAux },
  { path: '/privilegios/:id', component: PrivilegeAux },
  { path: '/login', component: Login },
  { path: '/logout', component: Logout }
]
